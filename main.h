#ifndef MAIN_H
#define MAIN_H

#include "pet.h"
#include "dog.h"
#include "cat.h"
#include <iostream>
#include <string>
using namespace std;

void displayDog(Dog *);

void displayCat(Cat *);


#endif