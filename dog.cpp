#include "dog.h"

//Dog class' mutator function definition.
void Dog::setDogAkc(char yesOrNo){
    if ((yesOrNo == 'Y') || (yesOrNo == 'y')){
        akcRegistered = true;
    }
    else akcRegistered = false;
};


//Dog class' accessor function definition.
string Dog::getDogAkc(){
    string akcStatus;
    if(akcRegistered == true){
        akcStatus = "Yes";
    }
    else akcStatus = "No";
    return akcStatus;
};

//Doc class' method function definition.
void Dog::bark(char yesOrNo){
    if ((yesOrNo == 'Y') || (yesOrNo == 'y')){
        cout << getPetName() << " has barked.\n";
    }
    else cout << "\n";
};

//Dog class' constructors and destructor.
Dog::Dog(){
    setPetName("N/a");
    setPetAge(0);
    setDogAkc('n');
};
Dog::Dog(string name, int age, char yesOrNo){
    setPetName(name);
    setPetAge(age);
    setDogAkc(yesOrNo);
};
Dog::~Dog(){
};