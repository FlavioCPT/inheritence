
#include "main.h"

void displayDog(Dog *dogObject){

    cout << dogObject->getPetName() << endl;
    cout << dogObject->getPetAge() << endl;
    cout << dogObject->getDogAkc() << endl;
    cout << endl;

}

void displayCat(Cat *catObject){

    cout << catObject->getPetName() << endl;
    cout << catObject->getPetAge() << endl;
    cout << catObject->getCatFood() << endl;
    cout << endl;

}

