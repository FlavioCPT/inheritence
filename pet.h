#ifndef PET_H
#define PET_H

#include<string>
using namespace std;

class Pet{
public:
    // Constructors and Destructor function protoypes.
    Pet();
    Pet(string, int);
    ~Pet();

    //Variables used as vessels.
    string name;
    int age;

    //Getter function prototypes.
    string getPetName();
    int getPetAge();

    //Setter function prototypes.
    void setPetName(string);
    void setPetAge(int);
};
#endif
