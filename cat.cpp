#include "cat.h"

//Cat class' mutator function definition.
void Cat::setCatFood(string food){
    favoriteFood = food;
};


//Cat class' accessor function definition.
string Cat::getCatFood(){
    return favoriteFood;
};

//Cat class' method function definition.
void Cat::meow(string food){
    if (food == "N/a"){
        cout << getPetName() << " wants food.\n";
    }
    else cout << getPetName() << " wants " << getCatFood() << ".\n";
};

//Cat class' constructors and destructor.
Cat::Cat(){
    setPetName("N/a");
    setPetAge(0);
    setCatFood("N/a");
};
Cat::Cat(string name, int age){
    setPetName(name);
    setPetAge(age);
    setCatFood("N/a");
};
Cat::Cat(string name, int age, string food){
    setPetName(name);
    setPetAge(age);
    setCatFood(food);
};
Cat::~Cat(){
};