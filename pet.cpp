#include "pet.h"

//Pet class' mutator function definitions.
void Pet::setPetName(string obtainedName){
    name = obtainedName;
};
void Pet::setPetAge(int obtainedAge){
    age = obtainedAge;
};

//Pet class' accessor function definitions.
string Pet::getPetName(){
    return name;
};
int Pet::getPetAge(){
    return age;
};

//Pet class' constructors and destructor.
Pet::Pet(){
    setPetName("N/a");
    setPetAge(0);
};
Pet::Pet(string name, int age){
    setPetName(name);
    setPetAge(age);
};
Pet::~Pet(){
};