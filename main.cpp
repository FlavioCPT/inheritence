#include "main.h"

int main(){
    //Creation of a dog objects.
    Dog emptyDog;
    Dog dogWithEverything("Jake", 3, 'Y');

    //Creation of cat objects.
    Cat emptyCat;
    Cat catWithEverything("Fifi", 2, "Fish");

    //Function calls to print object contents.
    displayDog(&emptyDog);
    displayDog(&dogWithEverything);
    displayCat(&emptyCat);
    displayCat(&catWithEverything);
};