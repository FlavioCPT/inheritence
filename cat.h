#ifndef CAT_H
#define CAT_H

#include "pet.h"
#include <iostream>
#include<string>
using namespace std;

class Cat : public Pet{
public:
    // Constructors and Destructor function prototypes.
    Cat();
    Cat(string, int);
    Cat(string, int, string);
    ~Cat();

    //Getter function prototype.
    string getCatFood();

    //Setter function prototype.
    void setCatFood(string);

    //Class method function prototype.
    void meow(string);

private:
    //Variable used as vessel.
    string favoriteFood;
};
#endif