#ifndef DOG_H
#define DOG_H

#include "pet.h"
#include <iostream>
#include<string>
using namespace std;

class Dog : public Pet{
public:
    // Constructors and Destructor function prototypes.
    Dog();
    Dog(string, int, char);
    ~Dog();

    //Getter function prototype.
    string getDogAkc();

    //Setter function prototype.
    void setDogAkc(char);

    //Class method function prototype.
    void bark(char);

private:
    //Variable used as vessel.
    bool akcRegistered;
};
#endif